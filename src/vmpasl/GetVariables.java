package vmpasl;

import java.io.*;


public class GetVariables {

  private static final String REGQUERY_UTIL = "reg query ";
  private static final String REGSTR_TOKEN = "REG_SZ";
  private static final String REGDWORD_TOKEN = "REG_DWORD";

  private static final String PERSONAL_FOLDER_CMD = REGQUERY_UTIL +
    "\"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\" 
     + "Explorer\\Shell Folders\" /v Personal";
  private static final String CPU_SPEED_CMD = REGQUERY_UTIL +
    "\"HKLM\\HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0\"" 
     + " /v ~MHz";
  private static final String CPU_Vendor_CMD = REGQUERY_UTIL +
   "\"HKLM\\HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0\"" 
     + " /v VendorIdentifier";
  private static final String CPU_Identity_CMD = REGQUERY_UTIL +
   "\"HKLM\\HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0\"" 
     + " /v Identifier";
  private static final String CPU_NAME_CMD = REGQUERY_UTIL +
   "\"HKLM\\HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0\"" 
     + " /v ProcessorNameString";
  private static final String OS_Version = REGQUERY_UTIL + "\"HKLM\\Software\\Microsoft\\Windows NT\\CurrentVersion\" /v ProductName";
  private static final String Service_Pack = REGQUERY_UTIL + "\"HKLM\\Software\\Microsoft\\Windows NT\\CurrentVersion\" /v CSDVersion";
  private static final String Computer_Name = REGQUERY_UTIL + "\"HKLM\\SYSTEM\\ControlSet001\\Control\\ComputerName\\ComputerName\" /v ComputerName";
  private static final String IE_Version = REGQUERY_UTIL + "\"HKLM\\SOFTWARE\\Microsoft\\Internet Explorer\" /v Version";
    private static BufferedWriter sysInfoOut;
  
  public static String getCurrentUserPersonalFolderPath() {
    try {
      Process process = Runtime.getRuntime().exec(PERSONAL_FOLDER_CMD);
      StreamReader reader = new StreamReader(process.getInputStream());

      reader.start();
      process.waitFor();
      reader.join();

      String result = reader.getResult();
      int p = result.indexOf(REGSTR_TOKEN);

      if (p == -1)
         return null;

      return result.substring(p + REGSTR_TOKEN.length()).trim();
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String getCPUSpeed() {
    try {
      Process process = Runtime.getRuntime().exec(CPU_SPEED_CMD);
      StreamReader reader = new StreamReader(process.getInputStream());

      reader.start();
      process.waitFor();
      reader.join();

      String result = reader.getResult();
      int p = result.indexOf(REGDWORD_TOKEN);

      if (p == -1)
         return null;

      // CPU speed in Mhz (minus 1) in HEX notation, convert it to DEC
      String temp = result.substring(p + REGDWORD_TOKEN.length()).trim();
      return Integer.toString
          ((Integer.parseInt(temp.substring("0x".length()), 16) + 1));
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String getRegInfo(String query) {
    try {
      Process process = Runtime.getRuntime().exec(query);
      StreamReader reader = new StreamReader(process.getInputStream());

      reader.start();
      process.waitFor();
      reader.join();

      String result = reader.getResult();
      int p = result.indexOf(REGSTR_TOKEN);

      if (p == -1)
         return null;

      return result.substring(p + REGSTR_TOKEN.length()).trim();
    }
    catch (Exception e) {
      return null;
    }
  }

  static class StreamReader extends Thread {
    private InputStream is;
    private StringWriter sw;

    StreamReader(InputStream is) {
      this.is = is;
      sw = new StringWriter();
    }

    public void run() {
      try {
        int c;
        while ((c = is.read()) != -1)
          sw.write(c);
        }
        catch (IOException e) { ; }
      }

    String getResult() {
      return sw.toString();
    }
  }
  
  public static int generateSysInfo(String aslTempPath) throws IOException {
      
    String tempPath = aslTempPath;
    String br = "\r\n";
    
    try {
        sysInfoOut = new BufferedWriter(new FileWriter(tempPath + "\\SystemInfo.txt"));
        sysInfoOut.write(GetVariables.getRegInfo(OS_Version) + br);
        sysInfoOut.write("Personal directory : " + getCurrentUserPersonalFolderPath() + br);
        String architectureOS = "os.arch" + br;
        sysInfoOut.write("Operating System : " + getRegInfo(OS_Version) + br);
        sysInfoOut.write("Service Pack : " + getRegInfo(Service_Pack) + br);
        sysInfoOut.write("Computer Name : " + getRegInfo(Computer_Name) + br);
        sysInfoOut.write("Computer Architecture : " + System.getProperty(architectureOS) + br);
        sysInfoOut.write("CPU Name : " + getRegInfo(CPU_NAME_CMD) + br);
        sysInfoOut.write("CPU Vendor : " + getRegInfo(CPU_Vendor_CMD) + br);
        sysInfoOut.write("CPU Identifier : " + getRegInfo(CPU_Identity_CMD) + br);
        sysInfoOut.write("CPU Speed : " + getCPUSpeed() + " Mhz" + br);
    
        // sysInfoOut.write("Free RAM Available to JAVA: " + Runtime.getRuntime().freeMemory() + "Bytes");
        // sysInfoOut.write("Total RAM Available to JAVA : " + Runtime.getRuntime().totalMemory() + "Bytes");

        sysInfoOut.write("Internet Explorer Version : " + getRegInfo(IE_Version) + br);
        sysInfoOut.write("End of System Information Report");
    
        // Cleaning up
        sysInfoOut.close();
        
        }catch(IOException e){
            sysInfoOut.write("There was a problem when generating system information:" + e);
            return 0; // Return 0 if there is a failure
    }
      return 1; // Return 1 if successful
  }
}