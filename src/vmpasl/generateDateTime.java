package vmpasl;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Description:
 * This class has methods that can be called to create the Date/Time stamps
 * needed for file names, etc.
 * 
 * @author cnellis
 * @version 1.0
 */
public class generateDateTime {
    
    public static String getZipStamp() {
    
        Date currDate = new Date();
        SimpleDateFormat dateStamp = new SimpleDateFormat("yyyy-MM-dd-HHmm");
        String zipStamp = dateStamp.format(currDate);
        
        return zipStamp;
}
    
    // Currently the same as the zipStamp return above, but broke it out in case of future changes
    public static String getManifestStamp() {
        
        Date currDate = new Date();
        SimpleDateFormat dateStamp = new SimpleDateFormat("yyyy-MM-dd-HHmm");
        String manifestStamp = dateStamp.format(currDate);
        
        return manifestStamp;
    }
}
